$(document).ready(function () {

    $('#userdiv').hide();
    $('#header').hide();

    var socket = io();

    $('form').submit(function () {
        console.log('registering!');
        socket.emit('register', $('#name').val());
        $('#name').val('');

        $('#userdiv').show();
        $('#header').show();
        // laadt nieuw pg met op trollface!
        $('#contentdiv').load("/clicker.html");

        return false;
    });

    $('body').on('click', '#trollface', function () {
        socket.emit('click', '');
    });

    socket.on('register', function (msg) {
        $('#userlist').append($('<li>').text(msg));
    });

    socket.on('player', function (msg) {
        var res = msg.split(";");

        var playerName = res[0];
        var clickCount = res[1];

        $('#userlist > li:contains(' + playerName + ')').replaceWith('<li>' + playerName + ' : ' + clickCount + '</li>');

        var items = $('#userlist > li').get();
        items.sort(function (a, b) {
            
            var keyA = (($(a).text()).replace(/ /g,'')).split(':')[1];
            var keyB = (($(b).text()).replace(/ /g,'')).split(':')[1];
                        
            return keyB-keyA;
        });

        var ul = $('#userlist');
        $.each(items, function (i, li) {
            ul.append(li);
        });
    });

    socket.on('state', function (msg) {
        var res = msg.split(":");
        var globalClicks = res[0];
        var totalCount = res[1];
        var trollHealth = res[2];
        var vic = res[3];
        
        $('#numberOfClicks').replaceWith('<h3 id="numberOfClicks"> Current : ' + totalCount + ' / ' + trollHealth +'</h3>');
        $('#serverClicks').replaceWith('<h3 id="serverClicks"> Total : ' + globalClicks + '</h3>');
        $('#trollLevel').replaceWith('<h3 id="trollLevel"> Troll Level : ' + vic + '</h3>');

        $('#health').attr('value', totalCount);
        $('#health').attr('max', trollHealth);

    });
    
    socket.on('victory', function(msg) {
        console.log('You have beaten a troll!');
    });

    socket.on('disconnect', function (msg) {
        $('#userlist > li:contains(' + msg + ')').remove();
    });

    socket.on('victory', function (msg) {
        console.log(msg);
    });


});

