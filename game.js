var totalClicks = 0;
var trollHealth = 5;
var numberOfVictories = 0;
var globalClicks = 0;

function click() {
    totalClicks++;
    globalClicks++;
};

function checkVictory() {
    if (totalClicks >= trollHealth) {
        reset();
        return true;
    } else {
        return false;
    }
};

function reset() {
    totalClicks = 0;
    trollHealth = trollHealth * 2;
    numberOfVictories++;
};

function state() {
    return globalClicks+':'+totalClicks+':'+trollHealth+':'+numberOfVictories;
};

// Functions which will be available to external callers
module.exports.click = click;
module.exports.checkVictory = checkVictory;
module.exports.state = state;


