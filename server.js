var game = require("./game.js");

var players = [];
var sockets = [];
var clickCount = [];
var playercount = 0;

function makeSocketServer(serv) {

    var io = require('socket.io').listen(serv);

    io.on('connection', function (socket) {
        console.log('[SERVER] a user connected');

        socket.on('disconnect', function () {
            console.log('[SERVER] user disconnected');
            var i = sockets.indexOf(socket);
            io.emit('disconnect', players[i]);
            players.splice(i, 1);
            sockets.splice(i, 1);
            clickCount.splice(i, 1);
        });

        socket.on('register', function (msg) {
            console.log('[SERVER] Register : ' + msg);

            sendGameState(socket, io);
            //bij reg -> stuur de huidge lijst van users:
            for (var i = 0; i < players.length; i++) {
                io.to(socket.id).emit('register', players[i]);
            }

            playercount = players.length;
            io.emit('register', msg);
            players[playercount] = msg;
            sockets[playercount] = socket;
            clickCount[playercount] = 0;
        });

        socket.on('click', function (msg) {
            
            console.log('message from ' + socket.id);
            game.click();
            var i = sockets.indexOf(socket);
            clickCount[i]++;

            var victory = game.checkVictory();

            if (victory) {
                io.emit('victory');
                io.emit('state', game.state());
            }

            //Send the clickCount + list of clicks per players to clients. 
            io.emit('state', game.state());

            if (playercount > 0) {
                for (var i = 0; i < players.length; i++) {
                    io.emit('player', players[i] + ";" + clickCount[i]);
                }
            }
        });

    });

    function sendGameState(socket, io) {
        io.to(socket.id).emit('state', game.state());
    };

}
// Functions which will be available to external callers
module.exports.makeSocketServer = makeSocketServer;